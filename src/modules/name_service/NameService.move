address 0xB25011a1512f326A4CFa1A259e6d7fC4 {
module NameService {
    use 0xB25011a1512f326A4CFa1A259e6d7fC4::SortedLinkedList::{Self, EntryHandle};
    use 0x1::Block;
    use 0x1::Signer;
    use 0x1::Vector;

    //TODO use constants when Move support constants, '5' is used for example
    const EXPIRE_AFTER: u64 = 5;

    struct Expiration has key, store {
        expire_on_block_number: vector<u64>
    }

    public fun entry_handle(addr: address, index: u64): EntryHandle {
        SortedLinkedList::entry_handle(addr, index)
    }

    public fun initialize(account: &signer) {
        let sender = Signer::address_of(account);
        assert(sender == 0xB25011a1512f326A4CFa1A259e6d7fC4, 8000);

        SortedLinkedList::create_new_list<vector<u8>>(account, Vector::empty());
        move_to<Expiration>(account, Expiration { expire_on_block_number: Vector::singleton(0u64)});
    }

    fun add_expirtation(account: &signer) acquires Expiration {
        let sender = Signer::address_of(account);
        let current_block = Block::get_current_block_number();
        if (!exists<Expiration>(sender)) {
            move_to<Expiration>(account, Expiration {expire_on_block_number: Vector::singleton(current_block + EXPIRE_AFTER)});
        } else {
            let expire_vector_mut = &mut borrow_global_mut<Expiration>(sender).expire_on_block_number;
            Vector::push_back<u64>(expire_vector_mut, current_block + EXPIRE_AFTER);
        };
    }

    public fun add_name(account: &signer, name: vector<u8>, prev_entry: EntryHandle) acquires Expiration {
        SortedLinkedList::insert_node(account, name, prev_entry);
        Self::add_expirtation(account);
    }

    public fun get_name_for(entry: EntryHandle): vector<u8> {
        SortedLinkedList::get_data<vector<u8>>(entry)
    }

    fun remove_expiration(entry: EntryHandle) acquires Expiration {
        let account_address = SortedLinkedList::get_addr(copy entry);
        let index = SortedLinkedList::get_index(entry);
        let expire_vector_mut = &mut borrow_global_mut<Expiration>(account_address).expire_on_block_number;
        Vector::remove<u64>(expire_vector_mut, index);
        if (Vector::is_empty<u64>(expire_vector_mut)) {
            let Expiration { expire_on_block_number } = move_from<Expiration>(account_address);
            Vector::destroy_empty(expire_on_block_number);
        }
    }
    public fun remove_entry_by_entry_owner(account: &signer, entry: EntryHandle) acquires Expiration {
        SortedLinkedList::remove_node_by_node_owner<vector<u8>>(account, copy entry);
        Self::remove_expiration(entry);
    }

    public fun remove_entry_by_service_owner(account: &signer, entry: EntryHandle) acquires Expiration {
        SortedLinkedList::remove_node_by_list_owner<vector<u8>>(account, copy entry);
        Self::remove_expiration(entry);
    }

    public fun find_position_and_insert(account: &signer, name: vector<u8>, head: EntryHandle): bool acquires Expiration {
        if (SortedLinkedList::find_position_and_insert<vector<u8>>(account, name, head)) {
            Self::add_expirtation(account);
            return true
        } else {
            return false
        }
    }

    public fun is_head_entry(entry: EntryHandle): bool {
		SortedLinkedList::is_head_node<vector<u8>>(&entry)
    }

    public fun expire_on_block_number(entry: EntryHandle): u64 acquires Expiration {
        let addr = SortedLinkedList::get_addr(copy entry);
        let index = SortedLinkedList::get_index(entry);
        let expire_vector = *&borrow_global<Expiration>(addr).expire_on_block_number;
        *Vector::borrow<u64>(&expire_vector, index)
    }

    public fun is_expired(entry: EntryHandle): bool acquires Expiration {
        let current_block_number = Block::get_current_block_number();
        current_block_number > expire_on_block_number(entry)
    }

}
}
